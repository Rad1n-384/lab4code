package linearalgebra;

//Radin Democri
// 2244552

public final class Vector3d {
    public static void main(String[] args) {
        Vector3d vector1 = new Vector3d(1, 2, 3);
        Vector3d vector2 = new Vector3d(3, 4, 5);
        System.out.println("vector 1 magnitude is : " + vector1.magnitude());
        System.out.println("dot product of vector 1 and vector 2 are: " + dotProduct(vector1, vector2));
        System.out.println("the resulting vector of vector 1 and 2 is " + toString(add(vector1, vector2)));

    }

    double x;
    double y;
    double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;

    }

    public double getZ() {
        return z;
    }

    public double magnitude() {
        double magnitude = Math.sqrt(Math.pow(getX(), 2) + Math.pow(getY(), 2) + Math.pow(getZ(), 2));
        return magnitude;
    }

    public static double dotProduct(Vector3d vector1, Vector3d vector2) {
        double dotProduct = (vector1.getX() * vector2.getX() + vector1.getY() * vector2.getY()
                + vector1.getZ() * vector2.getZ());
        return dotProduct;
    }

    public static Vector3d add(Vector3d vector1, Vector3d vector2) {
        double resultx = vector1.getX() + vector2.getX();
        double resulty = vector1.getY() + vector2.getY();
        double resultz = vector1.getZ() + vector2.getZ();
        Vector3d resultVector = new Vector3d(resultx, resulty, resultz);
        return resultVector;
    }

    public static String toString(Vector3d resultVector) {
        return "Vector: (" + resultVector.getX() + "," + resultVector.getY() + "," + resultVector.getZ() + ")";
    }
}
