package linearalgebra;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Vector3dTest {
    // Radin Democri
    // 2244552
    @Test
    public void testGetters() {
        Vector3d vector = new Vector3d(1.0, 2.0, 3.0);

        double x = vector.getX();
        double y = vector.getY();
        double z = vector.getZ();

        double expectedX = 1.0;
        double expectedY = 2.0;
        double expectedZ = 3.0;

        assertEquals(expectedX, x, 0.00);
        assertEquals(expectedY, y, 0.00);
        assertEquals(expectedZ, z, 0.00);
    }

    @Test
    public void testMagnitude() {
        Vector3d vector = new Vector3d(1.0, 2.0, 3.0);

        double magnitude = vector.magnitude();
        double expectedMagnitude = 3.7416573867739413;

        assertEquals(expectedMagnitude, magnitude, 0.00);
    }

    @Test
    public void testdotProduct() {
        Vector3d vector1 = new Vector3d(1, 2, 3);
        Vector3d vector2 = new Vector3d(3, 4, 5);

        double dotProduct = Vector3d.dotProduct(vector1, vector2);
        double expectedDotProduct = 26.0;

        assertEquals(expectedDotProduct, dotProduct, 0.00);
    }

    @Test
    public void testAdd() {
        Vector3d vector1 = new Vector3d(1, 2, 3);
        Vector3d vector2 = new Vector3d(3, 4, 5);

        Vector3d resultVector = Vector3d.add(vector1, vector2);

        double x = resultVector.getX();
        double y = resultVector.getY();
        double z = resultVector.getZ();

        double expectedX = 4.0;
        double expectedY = 6.0;
        double expectedZ = 8.0;

        assertEquals(expectedX, x, 0.00);
        assertEquals(expectedY, y, 0.00);
        assertEquals(expectedZ, z, 0.00);
    }
}
